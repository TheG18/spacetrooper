﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour 
{
	public GameObject PlayerBulletGo;//this is out player's prefab
	public GameObject bulletPosition01;
	public GameObject bulletPosition02;
	public GameObject Explosion;//this is our prefab

	public float speed;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//fire bullets when the spacebar is pressed
		if (Input.GetKeyDown ("space")) 
		{
			//instantiate the first bullet
			GameObject bullet01 = (GameObject)Instantiate (PlayerBulletGo);
			bullet01.transform.position = bulletPosition01.transform.position;//set the bullet initial position

			//instantiate the second bullet
			GameObject bullet02 = (GameObject)Instantiate (PlayerBulletGo);
			bullet02.transform.position = bulletPosition02.transform.position;//set the bullet initial position
		}

		float x = Input.GetAxisRaw ("Horizontal");//the value will be -1, 0, or 1 (for left, no input, and right)
		float y = Input.GetAxisRaw ("Vertical");//the value will be -1, 0, or 1 (for down, no input, and up)

		//now based on the input we compete a direction vector, and we normalize it to get a unit vctor
		Vector2 direction = new Vector2 (x, y).normalized;

		//now we call the function that computes and sets the player's position
		Move (direction);
	}

	void Move(Vector2 direction)
	{
		//find the screen limits to the player's movement(left, right, top, and bottom edges of the screen)
		Vector2 min = Camera.main.ViewportToWorldPoint (new Vector2 (0, 0)); //this is the bottom-left point (corner) of the screen
		Vector2 max = Camera.main.ViewportToWorldPoint (new Vector2 (1, 1)); //this is the top-right point (corner) of the screen

		max.x = max.x - 0.225f; //subtract the playey sprite half width
		min.x = min.x + 0.225f; //add the player sprite half width

		max.y = max.y - 0.285f; //subtract the player sprite half height
		min.y = min.y + 0.285f; //add the player sprite half height

		//Get the player's current position
		Vector2 pos = transform.position;

		//Calculate the new position
		pos += direction * speed * Time.deltaTime;

		//Make sure the new position is not outside the screen
		pos.x = Mathf.Clamp (pos.x, min.x, max.x);
		pos.y = Mathf.Clamp (pos.y, min.y, max.y);

		//Update the player's position
		transform.position = pos;
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		//Detect collisions of the player ship with an enemy ship, or with an enemy bullet
		if ((col.tag == "EnemyShipTag") || (col.tag == "EnemyBulletTag")) 
		{
			PlayExplosion();

			Destroy(gameObject);//Destroy the player's ship
		}
	}

	//Function to instantiate an explosion
	void PlayExplosion()
	{
		GameObject explosion = (GameObject)Instantiate (Explosion);

		//set the position of the explosion
		explosion.transform.position = transform.position;
	}
}
